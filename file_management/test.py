import os
import pytest
from unittest.mock import patch, mock_open
from main import get_file_metadata

class TestFileMetadata:
    """
    Test class for the get_file_metadata function in the main module.
    """

    @pytest.fixture
    def mock_os_walk(self):
        """
        Pytest fixture that mocks os.walk.
        """
        with patch('os.walk') as mock_walk:
            mock_walk.return_value = [
                ('/path/to/test/directory', (), ('file1.txt', 'file2.txt')),
            ]
            yield mock_walk

    @pytest.fixture
    def mock_os_path(self):
        """
        Pytest fixture that mocks os.path methods.
        """
        with patch('os.path.getsize') as mock_getsize, \
             patch('os.path.getmtime') as mock_getmtime, \
             patch('os.path.join') as mock_join:
            mock_getsize.return_value = 100
            mock_getmtime.return_value = 1633024800.0
            mock_join.side_effect = lambda a, b: a + '/' + b
            yield

    @pytest.mark.usefixtures('mock_os_walk', 'mock_os_path')
    def test_get_file_metadata(self, capsys):
        """
        Test case for the get_file_metadata function.
        """
        get_file_metadata('/path/to/test/directory')
        captured = capsys.readouterr()
        expected_output = (
            "{'filename': 'file1.txt', 'path': '/path/to/test/directory/file1.txt', 'size': 100, 'last_modified': 1633024800.0}\n"
            "{'filename': 'file2.txt', 'path': '/path/to/test/directory/file2.txt', 'size': 100, 'last_modified': 1633024800.0}\n"
        )
        assert captured.out == expected_output