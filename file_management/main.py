import os

def get_file_metadata(directory):
    for foldername, subfolders, filenames in os.walk(directory):
        for filename in filenames:
            filepath = os.path.join(foldername, filename)
            metadata = {
                'filename': filename,
                'path': filepath,
                'size': os.path.getsize(filepath),
                'last_modified': os.path.getmtime(filepath)
            }
            print(metadata)

get_file_metadata(os.path.dirname(os.path.abspath(__file__)))