import pytest
import requests
from main import scrape_top_news

class TestScrapeTopNews:
    @pytest.fixture
    def setup(self):
        url = 'https://news.ycombinator.com'
        response = requests.get(url)
        self.html = response.text

    def test_scrape_top_news(self, setup):
        result = scrape_top_news(self.html)
        assert isinstance(result, dict)
        for key, value in result.items():
            assert isinstance(key, str)
            assert isinstance(value, str)
            assert value.startswith('http')