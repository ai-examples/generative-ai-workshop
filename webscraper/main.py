import requests
from bs4 import BeautifulSoup

# URL of the site to scrape
url = 'https://news.ycombinator.com'

# Send a GET request to the URL
response = requests.get(url)

# Parse the HTML content of the page using BeautifulSoup
soup = BeautifulSoup(response.text, 'html.parser')

# Find all the news headlines
headlines = soup.find_all('tr', class_='athing')

# Dictionary to store headlines where key is the rank and value is the headline text
news_headlines = {}

# Loop through each headline, extract and store it in the dictionary
for headline in headlines:
    rank = headline.find('span', class_='rank').text.strip('.')
    title = headline.find('span', class_='titleline').text
    news_headlines[rank] = title

# Output the dictionary
print(news_headlines)
