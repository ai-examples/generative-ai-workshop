## Initial Prompt
```
Create a web application in Python that shows charts of the following stocks: RIVN, TSLA, NKLA, FSR
```

## Iterative Prompt
Remember that it still has context from the last request to reference to "the code" will be valid
```
Modify the code to enable me to select a date range
```